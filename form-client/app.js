'use strict';
var serviceBase = 'http://server.lc/'
var App = angular.module('App', [
  'ngRoute'
  , 'App.site'
  , 'App.responce'
  , 'ngAnimate'
]);
var site = angular.module('App.site', ['ngRoute'])
var application = angular.module('App.responce', ['ngRoute']);

App.config(['$routeProvider', function ($routeProvider) {
    $routeProvider.otherwise({
        redirectTo: '/form/index'
    });
}]);





App.directive("passwordVerify", function() {
   return {
      require: "ngModel",
      scope: {
        passwordVerify: '='
      },
      link: function(scope, element, attrs, ctrl) {
        scope.$watch(function() {
            var combined;
            if (scope.passwordVerify || ctrl.$viewValue) {
               combined = scope.passwordVerify + '_' + ctrl.$viewValue; 
            }                    
            return combined;
        }, function(value) {
            if (value) {
                ctrl.$parsers.unshift(function(viewValue) {
                    var origin = scope.passwordVerify;
                    if (origin !== viewValue) {
                        ctrl.$setValidity("passwordVerify", false);
                        return undefined;
                    } else {
                        ctrl.$setValidity("passwordVerify", true);
                        return viewValue;
                    }
                });
            }
        });
     }
   };
});
App.directive('initializeProperty', function() {
  return {
      restrict: 'A',
      link: function (scope, element, attr) {
        element.val('');
      }
  };
});






'use strict';
application.config(['$routeProvider', function ($routeProvider) {
    $routeProvider
        .when('/form/index', {
            templateUrl: 'views/templates/index.html'
            , controller: 'index'
        })
        .when('/form/editform/:Id', {
            templateUrl: 'views/templates/editForm.html',
            controller: 'editform',
            resolve: {
                responce: function (services, $route) {
                    var Id = $route.current.params.Id;
                    return services.getForm(Id);
                }
            }
        })
        .when('/form/edit', {
            templateUrl: 'views/templates/edit.html',
            controller: 'edit'
        })
        .when('/form/perfom', {
            templateUrl: 'views/templates/perfom.html',
            controller: 'perfom'
        })
        .when('/form/save/:Id', {
            templateUrl: 'views/templates/save.html',
            controller: 'save', 
            resolve: {
                responce: function (services, $route) {
                    var Id = $route.current.params.Id;
                    return services.getResponceForSave(Id);
                }
            }
        })
        .otherwise({
            redirectTo: '/form/index'
        });
}]);

application.controller('index', ['$scope', '$http', 'services'
    , function ($scope, $http, services) {
            $scope.message = '';
            $scope.save = true;
            var m = [1];
            $scope.items = m;
            $scope.addStudetn = function(res){
                m.push(1);
            }
            $scope.saveSubmit = function(form){
                $scope.save = false;
                if (isEmpty($scope.form.password.$error) && isEmpty($scope.form.re_password.$error) ) 
                    var results = services.createData(form);   
            }
            $scope.submit = function(form){
                $scope.save = true;
                if (isEmpty($scope.form.$error)) 
                    var results = services.createData(form);
                
            }
            
}])
    .controller('save', ['$scope', '$http', 'services', '$location', 'responce'
    , function ($scope, $http, services, $location, responce) {
            $scope.saveData = responce.data;

}])
    .controller('perfom', ['$scope', '$http', 'services', '$location', 'responce'
    , function ($scope, $http, services, $location, responce) {
        

}])
    .controller('editform', ['$scope', '$http', 'services', '$location', 'responce', '$route'
    , function ($scope, $http, services, $location, responce, $route) {
        $scope.saving = true;
        var user = responce.data.data;
        $scope.stud = user.s;
        delete user.s;
        $scope.user = angular.copy(user);
        var hashId = $route.current.params.Id;
        $scope.addStudetns = function(res){
            $scope.stud.push(1);
        }
        $scope.update = function(data){
            console.log($scope.user);
            $scope.saving = false;
            var update = services.createData($scope.user, hashId);
        }
}])

    .controller('edit', ['$scope', '$http', 'services'
    , function ($scope, $http, services) {
        $scope.er = false;
        $scope.login = function(auth){
            var results = services.login(auth);
            $scope.er = true;
        }

}]);




'use strict';
application.factory("services", ['$http', '$location', '$route'
    , function ($http, $location, $route) {
        var obj = {};
        obj.getData = function () {
            return $http.get(serviceBase + 'datas');
        }
        obj.createData = function (data, hashId) {

            var action = hashId ? '?edit=' + hashId : '';

            return  $http.post(serviceBase + 'form' + action, data)
                .then(successHandler);

            function successHandler(result) {
                if (result.data.status == 'fully') {
                    $location.path('/form/perfom/');
                }else{
                    if (!action){
                        $location.path('/form/save/'+result.data.hash);
                    }else{
                        $location.path('/form/editform/'+hashId);
                    }
                    alert('Success');
                }
                
            }

        };
        obj.getDataElem = function (id) {
            return $http.get(serviceBase + 'datas/' + id);
        }
        obj.getForm = function (id){
            return $http.get(serviceBase + 'form?edit=' + id);
        }
        obj.login = function(data){
            return $http.post(serviceBase + 'form/edit', data).then(success);
            function success(res){
                if (res.data.status == 'success') {
                    $location.path('/form/editform/'+res.data.hash);
                }else
                    return res.data.status;
            }
        }
        obj.getResponceForSave = function(id){
            return  $http.get(serviceBase + 'form/save?id='+id);
        }
        obj.updateData = function (data) {
            return $http.put(serviceBase + 'datas/' + data.id, data)
                .then(successHandler)
                .catch(errorHandler);
            function successHandler(result) {
                $location.path('/data/index');
            }
            function errorHandler(result) {
                alert("Error data")
                $location.path('/data/update/' + data.id)
            }
        };
        return obj;
}]);




function isEmpty(obj) {
    if (obj == null) return true;
    if (obj.length > 0)    return false;
    if (obj.length === 0)  return true;
    for (var key in obj) {
        if (hasOwnProperty.call(obj, key)) return false;
    }
    return true;
}