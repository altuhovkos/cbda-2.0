'use strict';
application.config(['$routeProvider', function ($routeProvider) {
    $routeProvider
        .when('/admin/index', {
            templateUrl: 'views/templates/index.html'
            , controller: 'index'
        })
        .when('/admin/editform/:Id', {
            templateUrl: 'views/templates/editForm.html',
            controller: 'editform',
            resolve: {
                responce: function (services, $route) {
                    var Id = $route.current.params.Id;
                    return services.getForm(Id);
                }
            }
        })
        .when('/admin/edit', {
            templateUrl: 'views/templates/edit.html',
            controller: 'edit'
        })
        .when('/admin/perfom', {
            templateUrl: 'views/templates/perfom.html',
            controller: 'perfom'
        })
        .when('/admin/save/:Id', {
            templateUrl: 'views/templates/save.html',
            controller: 'save', 
            resolve: {
                responce: function (services, $route) {
                    var Id = $route.current.params.Id;
                    return services.getResponceForSave(Id);
                }
            }
        })
        .otherwise({
            redirectTo: '/admin/index'
        });
}]);