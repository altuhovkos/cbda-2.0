'use strict';
application.factory("services", ['$http', '$location', '$route'
    , function ($http, $location, $route) {
        var obj = {};
        obj.getData = function () {
            return $http.get(serviceBase + 'datas');
        }
        obj.createData = function (data, hashId) {

            var action = hashId ? '?edit=' + hashId : '';

            return  $http.post(serviceBase + 'form' + action, data)
                .then(successHandler);

            function successHandler(result) {
                if (result.data.status == 'fully') {
                    $location.path('/admin/perfom/');
                }else{
                    if (!action){
                        $location.path('/admin/save/'+result.data.hash);
                    }else{
                        $location.path('/admin/editform/'+hashId);
                    }
                    alert('Success');
                }
                
            }

        };
        obj.getDataElem = function (id) {
            return $http.get(serviceBase + 'datas/' + id);
        }
        obj.getForm = function (id){
            return $http.get(serviceBase + 'form?edit=' + id);
        }
        obj.login = function(data){
            return $http.post(serviceBase + 'form/edit', data).then(success);
            function success(res){
                if (res.data.status == 'success') {
                    $location.path('/admin/editform/'+res.data.hash);
                }else
                    return res.data.status;
            }
        }
        obj.getResponceForSave = function(id){
            return  $http.get(serviceBase + 'form/save?id='+id);
        }
        obj.updateData = function (data) {
            return $http.put(serviceBase + 'datas/' + data.id, data)
                .then(successHandler)
                .catch(errorHandler);
            function successHandler(result) {
                $location.path('/data/index');
            }
            function errorHandler(result) {
                alert("Error data")
                $location.path('/data/update/' + data.id)
            }
        };
        return obj;
}]);