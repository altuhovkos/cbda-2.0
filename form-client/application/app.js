function include( filename ) {
	var js = document.createElement('script');
	js.setAttribute('type', 'text/javascript');
	js.setAttribute('src', filename);
	js.setAttribute('defer', 'defer');
	document.getElementsByTagName('HEAD')[0].appendChild(js);
	var cur_file = {};
	cur_file[window.location.href] = 1;
	if (!window.php_js) window.php_js = {};
	if (!window.php_js.includes) window.php_js.includes = cur_file;
	if (!window.php_js.includes[filename]) {
		window.php_js.includes[filename] = 1;
	} else {
		window.php_js.includes[filename]++;
	}
	return window.php_js.includes[filename];
}




'use strict';
var serviceBase = 'http://server.lc/'
var App = angular.module('App', [
  'ngRoute'
  , 'App.site'
  , 'App.responce'
  , 'ngAnimate'
]);
var site = angular.module('App.site', ['ngRoute'])
var application = angular.module('App.responce', ['ngRoute']);

App.config(['$routeProvider', function ($routeProvider) {
    $routeProvider.otherwise({
        redirectTo: '/admin/index'
    });
}]);

include('application/controllers/MainController.js');
include('application/routePrivider.js');
include('application/functions.js');
include('application/directives/directives.js');
include('application/services.js');