-- phpMyAdmin SQL Dump
-- version 4.2.12deb2
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Ноя 04 2015 г., 17:29
-- Версия сервера: 5.6.27-0ubuntu0.15.04.1
-- Версия PHP: 5.6.4-4ubuntu6.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `service`
--

-- --------------------------------------------------------

--
-- Структура таблицы `data`
--

CREATE TABLE IF NOT EXISTS `data` (
`id` int(11) NOT NULL,
  `director_first_name` varchar(255) DEFAULT NULL,
  `director_middle_initial` varchar(255) DEFAULT NULL,
  `director_last_name` varchar(255) DEFAULT NULL,
  `home_phone` varchar(255) DEFAULT NULL,
  `address` text,
  `city` text,
  `state` varchar(255) DEFAULT NULL,
  `zip` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` text,
  `directors_main_instrument` text,
  `new_member` varchar(255) DEFAULT NULL,
  `school_name` varchar(255) DEFAULT NULL,
  `school_phone` varchar(255) DEFAULT NULL,
  `principals_name` varchar(255) DEFAULT NULL,
  `past_president` varchar(11) DEFAULT NULL,
  `fax` varchar(255) DEFAULT NULL,
  `school_address` varchar(255) DEFAULT NULL,
  `school_city` varchar(255) DEFAULT NULL,
  `school_state` varchar(255) DEFAULT NULL,
  `school_zip` varchar(26) DEFAULT NULL,
  `i_ll_help_with_auditions` text,
  `i_m_playing_in_reading_band` varchar(11) DEFAULT NULL,
  `do_not_publish_my_home_contact` varchar(11) DEFAULT NULL,
  `do_not_share_my_email` varchar(11) DEFAULT NULL,
  `membership_type` varchar(255) DEFAULT NULL,
  `annual_dues` varchar(11) DEFAULT NULL,
  `pre_registration_member` varchar(11) DEFAULT NULL,
  `pre_registration_non_member` varchar(11) DEFAULT NULL,
  `banquet` varchar(11) DEFAULT NULL,
  `banquet_attendings_number` varchar(11) DEFAULT NULL,
  `payment_options` varchar(255) DEFAULT NULL,
  `payment_status` varchar(11) NOT NULL DEFAULT '0',
  `status` varchar(255) NOT NULL DEFAULT 'save',
  `hash` text,
  `donat` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=108 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `students`
--

CREATE TABLE IF NOT EXISTS `students` (
`id` int(11) NOT NULL,
  `data_id` int(11) NOT NULL,
  `band` varchar(255) DEFAULT NULL,
  `instrument` varchar(255) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `middle_initial` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `home_phone` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `zip` varchar(26) DEFAULT NULL,
  `parent_email` varchar(255) DEFAULT NULL,
  `student_email` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `grade_year` int(11) DEFAULT NULL,
  `high_school_year` int(11) DEFAULT NULL,
  `give_my_name` varchar(255) DEFAULT NULL,
  `have_access_to_and_experience_playing_on_a_clarinet` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=416 DEFAULT CHARSET=utf8;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `data`
--
ALTER TABLE `data`
 ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `students`
--
ALTER TABLE `students`
 ADD PRIMARY KEY (`id`), ADD KEY `data_id` (`data_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `data`
--
ALTER TABLE `data`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=108;
--
-- AUTO_INCREMENT для таблицы `students`
--
ALTER TABLE `students`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=416;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
