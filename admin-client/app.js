function getCookie(name) {
  var matches = document.cookie.match(new RegExp(
    "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
  ));
  return matches ? decodeURIComponent(matches[1]) : undefined;
}

'use strict';
var serviceBase = 'http://server.lc/'
var App = angular.module('App', [
  'ngRoute'
  , 'App.site'
  , 'App.responce'
  , 'ngAnimate'
]);

var site = angular.module('App.site', ['ngRoute'])

var application = angular.module('App.responce', ['ngRoute']);

App.config(['$routeProvider', function ($routeProvider) {
    $routeProvider.otherwise({
        redirectTo: '/admin/index'
    });
}]);


App.controller('verify', ['$scope', '$location', function($scope, $location){
    if (!getCookie('loginize')) {
        $location.path('/admin/login');
    }
}]);




'use strict';
application.config(['$routeProvider', function ($routeProvider) {
    $routeProvider
        .when('/admin/index', {
            templateUrl: 'views/templates/index.html'
            , controller: 'index'
        })
        .when('/admin/login', {
            templateUrl: 'views/templates/login.html'
            , controller: 'login'
        })
        .when('/admin/create', {
            templateUrl: 'views/templates/create.html'
            , controller: 'create'
            , resolve: {
                responce: function (services, $route) {
                    return services.getData();
                }
            }
        })
        .when('/admin/update/:Id', {
            templateUrl: 'views/templates/update.html'
            , controller: 'update'
            , resolve: {
                responce: function (services, $route) {
                    var Id = $route.current.params.Id;
                    return services.getDataElem(Id);
                }
            }
        })
        .when('/admin/view/:Id', {
            templateUrl: 'views/templates/view.html'
            , controller: 'update'
            , resolve: {
                responce: function (services, $route) {
                    var Id = $route.current.params.Id;
                    return services.getDataElem(Id);
                }
            }
        })
        .when('/admin/delete/:Id', {
            templateUrl: 'views/templates/index.html'
            , controller: 'delete'
        , })
        .otherwise({
            redirectTo: '/admin/index'
        });
}]);
application.controller('index', ['$scope', '$http', 'services'
	, function ($scope, $http, services) {
            $scope.message = '';
            services.getData()
                .then(function (data) {
                    $scope.data = data.data;
                });
            $scope.deleteData = function (id) {
                if (confirm("Are you sure to delete element #" + id) == true && id > 0) {
                    services.deleteData(id);
                    $route.reload();
                }
            };
}])
    .controller('create', ['$scope', '$http', 'services', '$location', 'responce'
	, function ($scope, $http, services, $location, responce) {
            $scope.message = '';
            $scope.createData = function (responce) {
                var results = services.createData(responce);
            }
}])




    .controller('login', ['$scope', '$http', '$location', 'services'
    , function ($scope, $http, $location, services) {
            $scope.username = '';
            $scope.password = '';
            $scope.error = false;
            $scope.loginize = function (responce) {
                if (responce.username == 'admin' && responce.password == 'admin') {
                    document.cookie = "loginize=true";
                    $location.path('/admin/index');
                }else{
                    $scope.error = true;
                }
            }

}])




    .controller('update', ['$scope', '$http', '$routeParams', 'services', '$location', 'responce'
	, function ($scope, $http, $routeParams, services, $location, updateDataElem) {
            $scope.message = '';
            var original = updateDataElem.data;
            $scope.updateDataElem = angular.copy(original);
            $scope.isClean = function () {
                return angular.equals(original, $scope.updateDataElem);
            }
            $scope.updateData = function (data) {
                var results = services.updateData(data);
            }

            $http.get(serviceBase + 'students?id='+updateDataElem.data.id).success(function (data) {
                $scope.students = data;
            });

}]);

'use strict';
application.factory("services", ['$http', '$location', '$route'
	, function ($http, $location, $route) {
        var obj = {};
        obj.getData = function () {
            return $http.get(serviceBase + 'datas');
        }
        obj.createData = function (data) {
            return $http.post(serviceBase + 'datas', data)
                .then(successHandler)
                .catch(errorHandler);

            function successHandler(result) {
                $location.path('/data/index');
            }

            function errorHandler(result) {
                alert("Error data")
                $location.path('/data/create')
            }
        };
        obj.getDataElem = function (id) {
            return $http.get(serviceBase + 'datas/' + id);
        }

        obj.updateData = function (data) {
            return $http.put(serviceBase + 'datas/' + data.id, data)
                .then(successHandler)
                .catch(errorHandler);

            function successHandler(result) {
                $location.path('/data/index');
            }

            function errorHandler(result) {
                alert("Error data")
                $location.path('/data/update/' + data.id)
            }
        };
        obj.deleteData = function (id) {
            return $http.delete(serviceBase + 'datas/' + id)
                .then(successHandler)
                .catch(errorHandler);

            function successHandler(result) {
                $route.reload();
            }

            function errorHandler(result) {
                alert("Error data")
                $route.reload();
            }
        };
        return obj;
}]);
